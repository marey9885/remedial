// contenido del cache estatico
// contenido del cache inmutable
//instala el cache
//Estrategia de cache
//Eliminar cache
importScripts('js/sw-utils.js');

//Crear constantes para almacenar el cache
const ESTATICO_CACHE = 'static-v1';
const DINAMICO_CACHE = 'dinamico-v1';
const INMUTABLE_CACHE = 'inmutable-v1';

const APP_SHELL = [
  '/',
  'index.html',
  'style.css',
  'js/app.js',
  'js/sw-utils.js',
];

const APP_SHELL_INMUTABLE = [];

self.addEventListener('install', (event) => {
  const cacheStatic = caches
    .open(ESTATICO_CACHE)
    .then((cache) => cache.addAll(APP_SHELL));
  const cacheInmutable = caches
    .open(INMUTABLE_CACHE)
    .then((cache) => cache.addAll(APP_SHELL_INMUTABLE));

  event.waitUntil(Promise.all[(cacheStatic, cacheInmutable)]);
});

self.addEventListener('fetch', (event) => {
  const respuesta = caches.match(event.request).then((res) => {
    if (res) {
      return res;
    } else {
      return fetch(event.request).then((newRes) => {
        return actualizaCacheDinamico(DINAMICO_CACHE, event.request, newRes);
      });
    }
  });

  event.respondWith(respuesta);
});
